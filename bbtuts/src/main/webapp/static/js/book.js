var Book = Backbone.Model.extend({
	urlRoot : 'http://localhost:8080/bbtuts/rest/books',
	initialize : function() {
		this.on('change', function() {
			console.log('Model Changes detected:');
			if (this.hasChanged('name')) {
				console.log('The name attribute has changed');
				console.log('The name has changed from '
						+ this.previous('name') + ' to ' + this.get('name'));
			}
			if (this.hasChanged('year')) {
				console.log('The year has changed');
			}
			console.log('Changed attributes: ' + JSON.stringify(this.changed));
			console.log('Previous attributes: '
					+ JSON.stringify(this.previousAttributes()));
		});
		this.on('invalid', function(model, error) {
			console.log('**Validation Error : ' + error + '**');
		});
	},
	defaults : {
		name : 'Book Title',
		author : 'No One'
	},
	printDetails : function() {
		console.log(this.get('name') + ' by ' + this.get('author'));
	},
	validate : function(attrs) {
		if (attrs.year < 2000) {
			return 'Year must be after 2000';
		}
		if (!attrs.name) {
			return 'A name must be provided';
		}
	}
});

var EBook = Book.extend({
	parse : function(response, xhr) {
		response.bookType = 'ebook';
		return response;
	},
	getWebLink : function() {
		return 'http://www.apress.com/' + this.get('name');
	},
	printDetails : function() {
		console.log('An ebook');
		Book.prototype.printDetails.call(this);
	}
});

var Library = Backbone.Collection.extend({
	model : Book,
	url : 'http://localhost:8080/bbtuts/rest/books/',
	initialize : function() {
		console.log('Creating a new library collection');
		this.on('remove', function(removedModel, models, options) {
			console.log('element removed at ' + options.index);
		});
	},
	comparator : function(a, b) {
		return a.get('name') < b.get('name');
	},
	parse : function(response, xhr) {
		/* custom parse code here */
		return response;
	}
});

var libFetchCallbacks = {
	success : function(e) {
		console.log('Got data');
	},
	error : function(e) {
		console.log('Something went wrong');
	}
};

/*
 * when adding to collection: add() adds model to collection without persisting
 * (persist managed by model), Collection.create() adds model to collection and
 * persists
 */

LibraryView = Backbone.View.extend({
	initialize : function() {
		this.render();
	},
	render : function() {
		this.$el.html('Book Name: ' + this.model.get('name'));
		return this;
	}
});