var Book = java.type("com.wellsfromwales.bbtuts.model.Book");

var books = [
             {id: 98, author: 'Stephen King', title: 'The Shining', year: 1977},
             {id: 99, author: 'George Orwell', title: '1984', year: 1949}
             ];

function findBook(id) {
	for (var i = 0; i < books.length; ++i) {
		if (books[i].id === id) {
			var book = books[i];
			return new Book(book.id, book.author, book.title, book.year);
		}
	}
	
	return null;
}

function removeBook(id) {
	var bookIndex = 0;
	for (var i = 0; i < books.length; ++i) {
		if (books[i].id === id) {
			bookIndex = i;
		}
	}
	books.splice(bookIndex, 1);
}