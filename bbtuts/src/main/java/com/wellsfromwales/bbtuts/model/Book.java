package com.wellsfromwales.bbtuts.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="books")
public class Book {
	private int id;
	private String author;
	private String name;
	private int year;
	public Book() {
		super();
	}
	public Book(int id, String author, String name, int year) {
		super();
		this.id = id;
		this.author = author;
		this.name = name;
		this.year = year;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
}
