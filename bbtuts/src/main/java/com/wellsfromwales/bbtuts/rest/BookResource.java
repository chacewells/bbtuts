package com.wellsfromwales.bbtuts.rest;

import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.function.Supplier;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.stereotype.Component;

import com.wellsfromwales.bbtuts.model.Book;

@Component
@Path("/books")
public class BookResource {
	
	int lastId = 99;
	
	List<Book> books = ((Supplier<List<Book>>)() -> {
		List<Book> list = new LinkedList<>();
		list.add(new Book(98,"Stephen King", "The Shining", 1977));
		list.add(new Book(99, "George Orwell", "1984", 1949));
		return list;
	}).get();
	
	int getNewId() {
		return ++lastId;
	}
	
	Book findBook(int id) {
		for ( Book b : books) {
			if (b.getId() == id) {
				return b;
			}
		}
		return null;
	}
	
	int addBook(Book book) {
		book.setId(getNewId());
		books.add(book);
		return book.getId();
	}
	
	void updateBook(Book book) {
		removeBook(book.getId());
		books.add(book);
	}
	
	Book removeBook(int id) {
		for (ListIterator<Book> itr = books.listIterator(); itr.hasNext();) {
			Book b = itr.next();
			if (b.getId() == id) {
				itr.remove();
				return b;
			}
		}
		return null;
	}
	
	@GET
	@Path("{id}")
	@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	public Book getBook(@PathParam("id") int id) {
		return findBook(id);
	}
	
	@DELETE
	@Path("{id}")
	@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	public Book editBook(@PathParam("id") int id) {
		return removeBook(id);
	}
	
	@POST
	@Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	public Book saveBook(Book book) {
		addBook(book);
		return book;
	}
	
	@PUT
	@Path("{id}")
	@Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	public Response editBook(@PathParam("id") int id, Book book) {
		book.setId(id);
		updateBook(book);
		return Response.ok().build();
	}
	
	@GET
	@Path("/")
	@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	public Response all() {
		GenericEntity<List<Book>> entity = new GenericEntity<List<Book>>(books){};
		if ( !books.isEmpty() ) {
			return Response.ok(entity).build();
		} else {
			return Response.status(204).build();
		}
	}

}
